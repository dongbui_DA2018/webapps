var mongoose = require('mongoose');
var MONGO_URL = 'mongodb://10.96.100.110:27017/';
var DB_NAME = 'example-app-db';

if (process.env.MONGO_PORT) MONGO_URL = 'mongodb://10.96.100.110:27017/';

mongoose.connect(MONGO_URL + DB_NAME);

mongoose.connection.on('error', console.error.bind(console));

module.exports = mongoose.connection;
